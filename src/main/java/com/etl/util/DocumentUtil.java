package com.etl.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-04-18
 * Time: 10:04 AM
 */
public class DocumentUtil {


    /**
     * 把内容输出到指定文件
     *
     * @param filePath
     * @param content
     */
    public static void fileOutPut(String filePath, String content) throws IOException {
        File file = new File(filePath);
        if (!file.getParentFile().exists())
            file.getParentFile().mkdirs();
        Writer out = new FileWriter(file);
        out.write(content);
        out.close();

        System.out.println("FilePath is "+file.getAbsolutePath());
    }

}
