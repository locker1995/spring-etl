package com.etl.mongodb.pojo;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 *  自定义的Mongodb的分页查询pojo-page
 *
 * User: Locker1995
 * Date: 2018-03-26
 * Time: 1:41 PM
 */
public class MongodbPage {

    private String page;//分页对象信息Json-String

    private List<String> data;//Json格式的数据集合

    private String criterias;// 本次查询的条件

    public MongodbPage() {
    }

    public MongodbPage(String page, List<String> data) {
        this.page = page;
        this.data = data;
    }

    public MongodbPage(String page, List<String> data, String criterias) {
        this.page = page;
        this.data = data;
        this.criterias = criterias;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public String getCriterias() {
        return criterias;
    }

    public void setCriterias(String criterias) {
        this.criterias = criterias;
    }
}
