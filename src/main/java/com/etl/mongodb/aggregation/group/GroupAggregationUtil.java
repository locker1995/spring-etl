package com.etl.mongodb.aggregation.group;

import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 *  聚合通道-分组工具类
 *
 * User: Locker1995
 * Date: 2018-04-02
 * Time: 2:53 PM
 */
public class GroupAggregationUtil {


    private GroupOperation group;


    public static GroupOperation getGroupAggregationByStr(GroupAggregation group) throws Exception {

        GroupAggregationUtil util = new GroupAggregationUtil();

        if(group.getFields()==null){
            throw new Exception("must have Group fields!");
        }else{
            util.setGroupFields(group.getFields().toArray(new String[group.getFields().size()]));
        }


        if(group.getCount()!=null)
            util.setCount(group.getCount());

        if(group.getMethodColumns()!=null){
            for(MethodColumn methodColumn:group.getMethodColumns())
                util.setMethodColumn(methodColumn);
        }



        return util.getGroup();
    }

    public GroupOperation getGroup() {
        return group;
    }

    public void setGroupFields(String[] fields){

        //获得列
        group=group = Aggregation.group(fields);

    }

    public void setMethodColumn(MethodColumn methodColumn){

        String column = methodColumn.getColumn();

        String alias=methodColumn.getMethod()+"-"+methodColumn.getColumn();

        if(methodColumn.getMethod().equals("sum"))
            group=group.sum(column).as(alias);
        if(methodColumn.getMethod().equals("avg"))
            group=group.avg(column).as(alias);
        if(methodColumn.getMethod().equals("max"))
            group=group.max(column).as(alias);
        if(methodColumn.getMethod().equals("min"))
            group=group.min(column).as(alias);
        if(methodColumn.getMethod().equals("first"))
            group=group.first(column).as(alias);
        if(methodColumn.getMethod().equals("last"))
            group=group.last(column).as(alias);

    }

    private void setCount(String alias){
        group = group.count().as(alias);
    }

//    private void setSum(String column,String alias){
//        group.sum(column).as(alias);
//    }
//
//
//    private void setAvg(String column,String alias){
//       group.avg(column).as(alias);
//    }
//
//    private void setMax(String column,String alias){
//      group.max(column).as(alias);
//    }
//
//    private void setMin(String column,String alias){
//        group.min(column).as(alias);
//    }
//
//    private void setFirst(String column,String alias){
//        group.first(column).as(alias);
//    }
//
//    private void setLast(String column,String alias){
//        group.last(column).as(alias);
//    }

}
