package com.etl.mongodb.aggregation.group;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-04-03
 * Time: 2:46 PM
 */
public class GroupAggregation {

    private List<String> fields;//分组的列名

    private List<MethodColumn> methodColumns;//

    private String count;//alias

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<MethodColumn> getMethodColumns() {
        return methodColumns;
    }

    public void setMethodColumns(List<MethodColumn> methodColumns) {
        this.methodColumns = methodColumns;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }


    public void addGroupField(String field){
        if(fields==null){
            fields= new ArrayList<>();
        }

        fields.add(field);
    }

    public void addMethodColumn(MethodColumn methodColumn){

        if(methodColumns==null){
            methodColumns=new ArrayList<>();
        }

        methodColumns.add(methodColumn);

    }

}
