package com.etl.mongodb.aggregation.group;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 *
 *
 * User: Locker1995
 * Date: 2018-04-03
 * Time: 3:39 PM
 */
public class MethodColumn {

    private String method;

    private String column;

    public MethodColumn() {
    }

    public MethodColumn(String method, String column) {
        this.method = method;
        this.column = column;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }
}
