package com.etl.mongodb.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.query.Query;


/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 *  分页工具
 *
 * User: Locker1995
 * Date: 2018-03-26
 * Time: 9:42 AM
 */
public class PageUtil {


    /**
     *
     * 为Query对象设置分页条件
     *
     * @param query
     * @param pageJson
     */
    public static void setPageQuery(Query query, String pageJson){

        JSONObject wannaPage = JSONObject.parseObject(pageJson);

        if(wannaPage.containsKey("pageSize")&&wannaPage.containsKey("pageNumber")){

            int pageSize = wannaPage.getInteger("pageSize");

            int pageNumber = wannaPage.getInteger("pageNumber");

            query.limit(pageSize).skip(pageNumber*(pageSize-1));

        }

    }


    /**
     *
     * 封装分页对象
     *
     * @param count
     * @param pageJson
     * @return
     */
    public static JSONObject getPageObject(long count,String pageJson){

        JSONObject page = JSONObject.parseObject(pageJson);

        long pageSize=page.getLong("pageSize");

        //数据总数
        page.put("count",""+count);

        //总页数
        page.put("pageCount",""+count/pageSize);

        return page;
    }

}
