package com.etl.mongodb.dao;

import com.mongodb.DBObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-04-04
 * Time: 11:59 AM
 */
@Repository
public class CommonMongoDao extends MongodbDao<DBObject> {

    public CommonMongoDao(){}

    public CommonMongoDao(MongoTemplate mongoTemplate){
        this.mongoTemplate=mongoTemplate;
    }

    @Override
    protected Class<DBObject> getEntityClass() {
        return DBObject.class;
    }
}
