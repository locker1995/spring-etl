package com.etl.main;

import com.alibaba.fastjson.JSONObject;
import com.etl.util.CommandLineUtil;
import com.etl.jdbc.jdbc.JDBCService;
import com.etl.mongodb.dao.CommonMongoDao;
import com.mongodb.Mongo;
import org.apache.commons.cli.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * <p>
 * 把某个数据库某张表 导到某个mongodb的Collection里面去
 * <p>
 * User: Locker1995
 * Date: 2018-04-10
 * Time: 2:32 PM
 */
public class DatabaseToMongodb {

    public static void main(String[] args) {
//        String[] arg = {"-dt", "mysql",
//                "-ds", "localhost:3306",
//                "-u", "root",
//                "-p", "root",
//                "-t", "customer",
//                "-s", "mytest",
//                "-ms","localhost:27017",
//                "-md","mytest",
//                "-c","commonclitest"
//
//        };
        System.out.println("Welcome to use 'DatabaseToMongodb' app!");

        JSONObject argsJson = argsCommonsCLI(args);
        if (!argsJson.isEmpty()) {
            //        System.out.println(argsJson.isEmpty());
            //
            //        System.out.println(argsJson);
            Mongo mongo1 = new Mongo(argsJson.getString("mongoUrl"));
            CommonMongoDao commonMongoDao = new CommonMongoDao(new MongoTemplate(mongo1,argsJson.getString("mongoSchema")));
            JDBCService jdbc = new JDBCService(argsJson);
            String tableName = argsJson.getString("table");
            String collectionName = argsJson.getString("collectionName");
            long count = jdbc.count(tableName);
            System.out.println("总数据条数:" + count);
            long start = System.currentTimeMillis();
            if (count > 5000) {
                for (int i = 0; i < count % 5000; i++) {
                    long start1 = System.currentTimeMillis();
                    List<JSONObject> data = jdbc.findPageData(tableName, 2500, i);
                    long end1 = System.currentTimeMillis();
                    System.out.println(i + "....耗时" + (end1 - start1) + "ms-------");
                    long start2 = System.currentTimeMillis();
                    commonMongoDao.batchInsert(data, collectionName);
                    long end2 = System.currentTimeMillis();
                    System.out.println(i + "....耗时" + (end2 - start2) + "ms-------");
                }
            } else {
                List<JSONObject> data = jdbc.findAllFromTable(tableName);
                commonMongoDao.batchInsert(data, collectionName);
            }
            long end = System.currentTimeMillis();
            System.out.println("....总耗时" + (end - start) + "ms-------");
        }
    }

    public static JSONObject argsCommonsCLI(String[] args) {
        Options options = new Options();

        Option opt = new Option("dt", "dbType", true, "database type");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("ds", "dbServer", true, "database serverUrl");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("s", "dbSchema", true, "database schema name");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("u", "username", true, "database username");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("p", "password", true, "database password");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("t", "table", true, "you wanna operate table name");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("ms","mongoUrl", true,"mongodb service url");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("md","mongoSchema", true,"mongodb schema name");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("c", "collectionName", true, "mongodb collection name");
        opt.setRequired(true);
        options.addOption(opt);


        return CommandLineUtil.getArgs(args,options,"database to mongodb");
    }
}
