package com.etl.main;

import com.alibaba.fastjson.JSONObject;
import com.etl.util.CommandLineUtil;
import com.etl.jdbc.jdbc.JDBCService;
import com.etl.util.DocumentUtil;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-04-16
 * Time: 10:36 AM
 */

public class DatabaseRelationShip {

    public static void main(String[] args) throws IOException {
//        String[] arg = {
//                "-dt", "mysql",
//                "-ds", "localhost:3306",
//                "-u", "root",
//                "-p", "root",
//                "-s", "mytest"
//        };
        System.out.println("Welcome to use Database-ER-relationship APP!");
        JSONObject jsonArgs = DatabaseERJsonAPP(args);

        if(!jsonArgs.isEmpty()){

            JDBCService mysql = new JDBCService(jsonArgs);

            JSONObject erJson = mysql.schemaInfo();

            if(jsonArgs.containsKey("filePath")){
                DocumentUtil.fileOutPut(jsonArgs.getString("filePath"),erJson.toJSONString());
            }else{

                DocumentUtil.fileOutPut("/Database-ER-relationship-"+System.currentTimeMillis()+".txt",erJson.toJSONString());

            }
        }

    }


    public static JSONObject DatabaseERJsonAPP(String args[]){

        Options options = new Options();

        Option opt = new Option("dt", "dbType", true, "database type");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("ds", "dbServer", true, "database serverUrl");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("s", "dbSchema", true, "database schema name");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("u", "username", true, "database username");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("p", "password", true, "database password");
        opt.setRequired(true);
        options.addOption(opt);

        opt = new Option("f", "filePath", true, "Json file output path");
        opt.setRequired(false);
        options.addOption(opt);


        return CommandLineUtil.getArgs(args,options,"Database-ER-relationship APP");
    }

}
