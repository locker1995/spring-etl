package com.etl.main.test.projection;

import com.etl.mongodb.util.ProjectionUtil;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;

import java.text.ParseException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-05-07
 * Time: 2:59 PM
 */
public class Test3 {

    public static void main(String[] args) throws ParseException {

        String str="{\n" +
                "    \"projections\": [\n" +
                "        {\n" +
                "            \"target\": \"username\",\n" +
                "            \"alias\": \"用户名称\",\n" +
                "            \"method\": \"\",\n" +
                "            \"args\": [\n" +
                "                \"\"\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"target\": \"location\",\n" +
                "            \"alias\": \"substring1\",\n" +
                "            \"method\": \"substring\",\n" +
                "            \"args\": [\n" +
                "                \"5\"\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"target\": \"location\",\n" +
                "            \"alias\": \"substring2\",\n" +
                "            \"method\": \"substring\",\n" +
                "            \"args\": [\n" +
                "                \"5\",\n" +
                "                \"7\"\n" +
                "            ]\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        Mongo mongo1 = new Mongo("localhost:27017");
        MongoTemplate mongoTemplate = new MongoTemplate(mongo1,"mytest");

        ProjectionOperation projectionOperation = ProjectionUtil.getProjection(str);
        Aggregation aggregation = Aggregation.newAggregation(
                projectionOperation);
        AggregationResults aggRes = mongoTemplate.aggregate(aggregation, "users",DBObject.class);
        List<DBObject> listRes = aggRes.getMappedResults();
        for (DBObject object : listRes) {
            System.out.println(object);
        }

    }

}
