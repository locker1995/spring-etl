package com.etl.main.test.projection;

import com.etl.mongodb.dao.CommonMongoDao;
import com.etl.mongodb.util.ProjectionUtil;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;

import java.text.ParseException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-05-07
 * Time: 2:55 PM
 */
public class Test1 {


    public static void main(String[] args){

        String str="{\n" +
                "    \"projections\": [\n" +
                "        {\n" +
                "            \"target\": \"username\",\n" +
                "            \"alias\": \"用户名称\",\n" +
                "            \"method\": \"\",\n" +
                "            \"args\": [\n" +
                "                \"\"\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"target\": \"money\",\n" +
                "            \"alias\": \"money*number1\",\n" +
                "            \"method\": \"multiply\",\n" +
                "            \"args\": [\n" +
                "                \"number1\"\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"target\": \"number1\",\n" +
                "            \"alias\": \"money+number1\",\n" +
                "            \"method\": \"plus\",\n" +
                "            \"args\": [\n" +
                "                \"money\"\n" +
                "            ]\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        Mongo mongo1 = new Mongo("localhost:27017");
        MongoTemplate mongoTemplate = new MongoTemplate(mongo1,"mytest");

        try {
            ProjectionOperation projectionOperation = ProjectionUtil.getProjection(str);
            Aggregation aggregation = Aggregation.newAggregation(
                    projectionOperation);
            AggregationResults aggRes = mongoTemplate.aggregate(aggregation, "users",DBObject.class);
            List<DBObject> listRes = aggRes.getMappedResults();
            for (DBObject object : listRes) {
                System.out.println(object);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}
