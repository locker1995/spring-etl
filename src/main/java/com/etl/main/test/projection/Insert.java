package com.etl.main.test.projection;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-05-07
 * Time: 2:57 PM
 */
public class Insert {


    public static void main(String[] args){
        Mongo mongo1 = new Mongo("localhost:27017");
        MongoTemplate mongoTemplate = new MongoTemplate(mongo1,"mytest");
        List<DBObject> objects = new ArrayList<>();

        for(int i=0;i<100;i++){
            DBObject object = new BasicDBObject();
            object.put("username","Locker"+i);
            object.put("money",i*i);
            object.put("number1",i+i);
            object.put("location","locate"+i);
            object.removeField("class");
            objects.add(object);
        }
        mongoTemplate.insert(objects,"users");

    }

}
