package com.etl.jdbc.util;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-03-29
 * Time: 10:01 AM
 */
public class TimeUtil {

    /**
     *
     * Str -> Time
     * @param strDate
     * @return
     */
    public  static Time strToDate(String strDate) {
        String str = strDate;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Date d = null;
        try {
            d = format.parse(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Time time = new Time(d.getTime());
        return time.valueOf(str);
    }
}
