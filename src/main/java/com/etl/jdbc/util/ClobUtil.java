package com.etl.jdbc.util;

import javax.sql.rowset.serial.SerialClob;
import java.sql.Clob;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-03-29
 * Time: 9:54 AM
 */
public class ClobUtil {

    // String类型转换成Clob类型
    public static Clob StringToClob(final String string) throws SQLException {

        if(null == string || string.trim().length() == 0){
            return null;
        }

        Clob c = new SerialClob("abc".toCharArray());
        return c;
    }

}
