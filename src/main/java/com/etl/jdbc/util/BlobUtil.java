package com.etl.jdbc.util;

import javax.sql.rowset.serial.SerialBlob;
import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-03-29
 * Time: 9:56 AM
 */
public class BlobUtil {

    public static Blob strToBlob(String str) throws UnsupportedEncodingException, SQLException {

        return new SerialBlob(str.getBytes("UTF-8"));
    }

}
