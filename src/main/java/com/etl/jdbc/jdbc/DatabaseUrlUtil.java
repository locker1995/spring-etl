package com.etl.jdbc.jdbc;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Locker1995
 * Date: 2018-04-10
 * Time: 3:18 PM
 */
public class DatabaseUrlUtil {


    public static String getDatabaseUrl(String databaseType, String server, String databaseName) {

        StringBuffer sb = new StringBuffer();

        if ("mysql".equals(databaseType.toLowerCase())) {
            sb.append("jdbc:mysql://").append(server)
                    .append("/")
                    .append(databaseName)
                    .append("?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&rewriteBatchedStatements=true");
        }

        if ("oracle".equals(databaseType.toLowerCase())) {
            sb.append("jdbc:oracle:thin:@//")
                    .append(server)
                    .append("/").append(databaseName);
        }
        if ("sqlserver".equals(databaseType.toLowerCase())) {

            sb.append("jdbc:sqlserver://").
                    append(server).
                    append(";DatabaseName=").
                    append(databaseName);
        }

        return sb.toString();
    }

}
