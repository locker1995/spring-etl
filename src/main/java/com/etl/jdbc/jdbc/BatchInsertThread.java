package com.etl.jdbc.jdbc;

import com.alibaba.fastjson.JSONObject;
import com.etl.jdbc.util.TimeUtil;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * 批处理线程
 *
 * User: Locker1995
 * Date: 2018-03-29
 * Time: 4:23 PM
 */
public class BatchInsertThread extends Thread {

    private List<JSONObject> data ;

    private List<JSONObject> columns;

    private JdbcTemplate jdbcTemplate;

    private String sql;

    private int threadId;//线程编号


    public BatchInsertThread(String sql, List<JSONObject> data, List<JSONObject> columns, JdbcTemplate jdbcTemplate, int threadId) {
        this.sql=sql;
        this.data = data;
        this.columns = columns;
        this.jdbcTemplate = jdbcTemplate;
        this.threadId = threadId;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        this.jdbcTemplate.batchUpdate(this.sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                for(int j = 0;j<columns.size();j++){
                    JSONObject column = columns.get(j);
                    //列名
                    String columnName = column.getString("ColumnName");
                    int columnType = column.getInteger("ColumnType");
                    switch (columnType) {
                        case Types.VARCHAR :
                        case Types.NVARCHAR:
                            ps.setString(j+1,data.get(i).getString(columnName));
                            break;
                        case Types.DATE:
                            ps.setDate(j+1, Date.valueOf(data.get(i).getString(columnName)));
                            break;
                        case Types.DECIMAL:
                        case Types.BIGINT:
                        case Types.NUMERIC:
                            ps.setLong(j+1, data.get(i).getLong(columnName));
                            break;
                        case Types.FLOAT:
                            ps.setFloat(j+1, data.get(i).getFloat(columnName));
                            break;
                        case Types.DOUBLE:
                            ps.setDouble(j+1,data.get(i).getDouble(columnName));
                            break;
                        case Types.BOOLEAN:
                            ps.setBoolean(j+1,data.get(i).getBoolean(columnName));
                            break;
                        case Types.INTEGER:
                            ps.setInt(j+1,data.get(i).getInteger(columnName));
                            break;
                        case Types.TIME:
                            ps.setTime(j+1, TimeUtil.strToDate(data.get(i).getString(columnName)));
                            break;
                        default:
                            ps.setObject(j+1,data.get(i).get(columnName));
                            break;
                    }

                }

            }

            @Override
            public int getBatchSize() {
                return data.size();
            }
        });
        long end = System.currentTimeMillis();
        System.out.println("线程"+threadId+"耗时:"+(end-start)/1000+"s");
    }
}
